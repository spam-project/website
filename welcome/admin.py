from django.contrib import admin
from .models import Article, Citations


@admin.register(Article)
class ArticleAdmin(admin.ModelAdmin):
    list_display = ('article_id', 'title', 'authors', 'year', 'journal', 'editor')

@admin.register(Citations)
class CitationsAdmin(admin.ModelAdmin):
    list_display = ('timestamp', 'n')
