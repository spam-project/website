from django.shortcuts import render
from django.template import TemplateDoesNotExist
from django.http import Http404
from .models import Workshop

def workshops(request):
    context = {"workshops": Workshop.objects.order_by("-start")}
    return render(request, "workshop/workshops.html", context=context)

def workshop(request, year, month):
    workshop = Workshop.objects.filter(year=year).filter(month=month).first()

    if not workshop:
        raise Http404(f"Workshop not found for year {year} and month {month}")

    try:
        return render(request, f"workshop/workshop-{year:4d}-{month:02d}.html", context={"workshop": workshop})
    except TemplateDoesNotExist:
        return render(request, f"workshop/workshop-default.html", context={"workshop": workshop})
        
    
