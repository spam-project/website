
const gallery_element = document.getElementById('gallery-fs-modal');

if(gallery_element) {
  const gallery = new bootstrap.Modal();

  document.querySelectorAll('.gallery-fs-input').forEach(item => {
    item.addEventListener('click', event => {
      console.log("gallery trigger")

      // clean all active (otherwise it messes up with the carousel on second click)
      document.querySelectorAll('.carousel-item').forEach(i => {
	i.classList.remove("active");
      })
      console.log('gallery-fs-image-' + event.target.dataset["key"]);
      firstImage = document.getElementById('gallery-fs-image-' + event.target.dataset["key"]);
      firstImage.parentNode.classList.add("active");
      gallery.show()
    })
  })
} else {
  console.log("[gallery] gallery not found.")
}

document.querySelectorAll('.carousel-item').forEach(targetNode => {

  // Select the node that will be observed for mutations
  // console.log(targetNode)

  // Options for the observer (which mutations to observe)
  const config = { attributes: true };

  // Callback function to execute when mutations are observed
  const callback = function(mutationsList, observer) {
    // Use traditional 'for loops' for IE 11
    for(const mutation of mutationsList) {
      if(mutation.target.classList.contains('active')) {
	const img = mutation.target.firstElementChild
	if(img.src.includes("thumbnails")) {
	  img.src = img.src.replace("/thumbnails", "").replace("_large", "")
	}
      }
    }
  };

  // Create an observer instance linked to the callback function
  const observer = new MutationObserver(callback);

  // Start observing the target node for configured mutations
  observer.observe(targetNode, config);

  // Later, you can stop observing
  // observer.disconnect();

});
