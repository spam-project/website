from django.shortcuts import render
from spam.settings import DEBUG

def _403(request, exception=None):
    status = 200 if DEBUG else 403
    return render(request, '403.html', status=status)

def _404(request, exception=None):
    status = 200 if DEBUG else 404
    return render(request, '404.html', status=status)

def _500(request, exception=None):
    status = 200 if DEBUG else 500
    return render(request, '500.html', status=status)
