// -------------- //
// ENABLE TOOLTIP //
// -------------- //
const tooltipTriggerList = document.querySelectorAll('[data-bs-toggle="tooltip"]')
const tooltipList = [...tooltipTriggerList].map(tooltipTriggerEl => new bootstrap.Tooltip(tooltipTriggerEl))

// ----------------- //
// COPY TO CLIPBOARD //
// ----------------- //
document.querySelectorAll('.copy-clipboard').forEach(targetNode => {

  targetNode.addEventListener("click", function (event) {
    event.preventDefault()

    // put data-copy in clipboard
    navigator.clipboard.writeText(targetNode.dataset["copy"]);

    // show / hide popover
    const popover = new bootstrap.Popover(event.target);
    popover.show();
    setTimeout(function () { popover.hide() }, 1000);

  }, false);

});

const citations = document.getElementById('citations-list');
if(citations) {
  citations.addEventListener("click", function (event) {
    if(citations.classList.contains("citations-hidden")) {
      event.preventDefault();

      citations.classList = ["citations"];
      fetch("/citations").then(function (response) {
        return response.text();
      }).then(function (data) {
        citations.innerHTML = data;
      });
    }
  });
}
