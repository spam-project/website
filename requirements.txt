Django
requests
python-decouple
unidecode
django-json-widget
pre-commit
gunicorn
psycopg2-binary
# google-search-results
beautifulsoup4
django-thumbnails

