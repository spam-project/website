from django import template
import random
import os

register = template.Library()

@register.filter
def add_random_image(path):
    img = f"default-dev-{random.randint(0, 4)}.jpg"
    return os.path.join(path, img)


