from django.shortcuts import render
from django.db.models import Q
from spam.settings import N_ARTICLES
from .models import Article, Citations
from workshop.models import Workshop

def index(request):
    # workshops = Workshop.objects.filter(open_registration=True)
    workshops = Workshop.objects.filter(Q(open_registration=True) | Q(display_gallery=True))
    context = {"articles": Article.objects.order_by('-year')[:5], "citations": Citations.objects.last(), "N_ARTICLES": N_ARTICLES, "workshops": workshops}
    return render(request, "welcome/index.html", context=context)

def citations(request):
    context = {"articles": Article.objects.order_by('-year')[:N_ARTICLES]}
    return render(request, "welcome/articles.html", context=context)
