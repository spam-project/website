# Generated by Django 5.0.1 on 2024-02-27 09:01

import thumbnails.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('workshop', '0006_workshop_current'),
    ]

    operations = [
        migrations.CreateModel(
            name='Developer',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=126)),
                ('text', models.TextField()),
                ('image', thumbnails.fields.ImageField(blank=True, null=True, upload_to='developer-picture')),
                ('workshops', models.ManyToManyField(related_name='developers_attending', to='workshop.workshop')),
            ],
        ),
    ]
