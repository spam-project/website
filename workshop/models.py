from django.db import models
from thumbnails.fields import ImageField

class Workshop(models.Model):
    start = models.DateField()
    end = models.DateField()

    open_registration = models.BooleanField(default=False)
    display_gallery = models.BooleanField(default=False)
    
    year = models.IntegerField(default=1970)
    month = models.IntegerField(default=1)
    city = models.CharField(max_length=126, default="City")
    country = models.CharField(max_length=126, default="Country")

    image_title_0 = models.CharField(max_length=126, blank=True, null=True)
    image_0 = ImageField(upload_to='workshop-gallery', pregenerated_sizes=["large"], blank=True, null=True)
    image_title_1 = models.CharField(max_length=126, blank=True, null=True)
    image_1 = ImageField(upload_to='workshop-gallery', pregenerated_sizes=["large"], blank=True, null=True)
    image_title_2 = models.CharField(max_length=126, blank=True, null=True)
    image_2 = ImageField(upload_to='workshop-gallery', pregenerated_sizes=["large"], blank=True, null=True)
    image_title_3 = models.CharField(max_length=126, blank=True, null=True)
    image_3 = ImageField(upload_to='workshop-gallery', pregenerated_sizes=["large"], blank=True, null=True)
    
    def __str__(self):
        return f"Workshop in {self.city} ({self.year})"

    def get_pictures(self):
        pictures = [{
            "image": getattr(self, f"image_{i}"),
            "title": getattr(self, f"image_title_{i}")
        } for i in range(4)]
        pictures = [p for p in pictures if p["image"]]
        return pictures

    def get_developers(self):
        # return self.developers_attending.exclude(image="").order_by("name")
        return self.developers_attending.order_by("name")
    
class Developer(models.Model):
    name = models.CharField(max_length=126)
    text = models.TextField()
    image = ImageField(upload_to='developer-picture', pregenerated_sizes=["profile"], blank=True, null=True)
    link = models.CharField(max_length=126, blank=True, null=True)
    workshops = models.ManyToManyField(Workshop, related_name='developers_attending', blank=True)
    
    def __str__(self):
        return self.name
