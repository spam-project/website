"""
URL configuration for spam project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, register_converter
from django.conf import settings
from django.conf.urls.static import static
from django.views.generic.base import RedirectView
from django.conf.urls import handler403, handler404, handler500

import welcome.views
import workshop.views
import spam.views

class FourDigitYearConverter:
    regex = "[0-9]{4}"

    def to_python(self, value):
        return int(value)

    def to_url(self, value):
        return "%04d" % value

class TwoDigitMonthConverter:
    regex = "[0-9]{2}"

    def to_python(self, value):
        return int(value)

    def to_url(self, value):
        return "%02d" % value

register_converter(FourDigitYearConverter, "yyyy")
register_converter(TwoDigitMonthConverter, "mm")

urlpatterns = [
    # admin
    path("admin/", admin.site.urls),

    # welcome
    path("", welcome.views.index, name="index"),
    path("citations/", welcome.views.citations, name="citations"),

    # workshop
    path("workshops/", workshop.views.workshops, name="workshops"),
    path("workshops/<yyyy:year>/<mm:month>/", workshop.views.workshop, name="workshop"),

    # hack for QR code made before yyyy/mm
    path("workshops/2024/", RedirectView.as_view(url="/workshops/2024/10/")),

    # favicon
    path('favicon.ico', RedirectView.as_view(url=settings.STATIC_URL + '/favicon.ico')),

    # shortcuts
    path('chat', RedirectView.as_view(url="https://matrix.to/#/#spam:matrix.org"), name="chat"),
    path('git', RedirectView.as_view(url="https://gitlab.com/spam-project/spam"), name="git"),

        
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

    # test error pages on debug mode
    urlpatterns += [
        path('403/', spam.views._403),
        path('404/', spam.views._404),
        path('500/', spam.views._500)
    ]

    
handler403 = spam.views._403
handler404 = spam.views._404
handler500 = spam.views._500
