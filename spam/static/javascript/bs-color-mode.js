(() => {
  'use strict';

  const storedTheme = localStorage.getItem('theme');

  const autoTheme = () => {
    return window.matchMedia('(prefers-color-scheme: dark)').matches ? 'dark' : 'light';
  };

  const getPreferredTheme = () => {
    if (storedTheme) {
      return storedTheme;
    }

    return autoTheme()
  };

  const setTheme = function (theme) {
    const slider = document.querySelector("i#theme-slider");
    if (theme === 'auto' && window.matchMedia('(prefers-color-scheme: dark)').matches) {
      slider.classList.remove("bi-toggle-off");
      slider.classList.add("bi-toggle-on");
      document.documentElement.setAttribute('data-bs-theme', 'dark');
    } else {
      if (theme == "dark") {
        slider.classList.remove("bi-toggle-off");
        slider.classList.add("bi-toggle-on");
      } else {
        slider.classList.remove("bi-toggle-on");
        slider.classList.add("bi-toggle-off");
      }
      document.documentElement.setAttribute('data-bs-theme', theme);
    }
    localStorage.setItem('theme', theme);
    document.querySelector("#theme-value").innerHTML = theme;

    // style sheet
    const stylesheetToRemove = document.getElementById("codesheet");
    document.head.removeChild(stylesheetToRemove);

    const linkElement = document.createElement("link");
    linkElement.id = "codesheet"
    linkElement.rel = "stylesheet";
    linkElement.href = `https://cdnjs.cloudflare.com/ajax/libs/highlight.js/11.8.0/styles/atom-one-${theme == "auto" ? autoTheme() : theme}.min.css`;
    document.head.appendChild(linkElement);

    // change table style
    document.querySelectorAll("table").forEach(table => {
      console.log(table);
      table.classList.remove("table-dark")
      table.classList.remove("table-light")
      table.classList.add(`table-${theme == "auto" ? autoTheme() : theme}`);
      console.log(table);
    });
    // change image
    const images = document.querySelectorAll("img.dark-mode");
    [...images].forEach(image => {
      // console.log(image);

      image.src = image.src.split("__")[0].replace(".png", "") + `__${theme == "auto" ? autoTheme() : theme}.png`
    });
  };

  window.addEventListener('DOMContentLoaded', () => {
    setTheme(getPreferredTheme());

    document.querySelector('#theme-switcher').addEventListener('click', () => {
      const currentTheme = document.documentElement.getAttribute('data-bs-theme');
      setTheme(currentTheme == "dark" ? "light" : "dark");
    });
    document.querySelector('#theme-auto').addEventListener('click', () => {
      setTheme("auto");
    });
  });

})();
