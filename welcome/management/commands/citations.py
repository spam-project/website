from django.core.management.base import BaseCommand
import json
import requests
from bs4 import BeautifulSoup
from spam.settings import GS_URI
from unidecode import unidecode
import re
import time

from welcome.models import Article, Citations

DOIISH_REPLACE = [".pdf", ".PDF", "/meta"]
TITLE_REPLACE = ["[HTML]"]

def gs(start=0):
    papers = []
    n = 0

    try:
        response = requests.get(GS_URI + f"&start={start}")
        if response.status_code == 200:
            soup = BeautifulSoup(response.text, 'html.parser')

            # Extract and print article results
            try:
                n = soup.find_all('div', class_="gs_ab_mdw")[-1].get_text().split()[1]
                print(f"Number of citations: {n}")
            except Exception as e:
                print(f"Could not parse number of citations: {e}")
                n = 0

            results = soup.find_all('div', class_='gs_ri')
            for result in results:
                # print(result)
                title = unidecode(result.find('h3', class_='gs_rt').get_text())
                for r in TITLE_REPLACE:
                    title = title.replace(r, "")
                title = title.strip()
                link = result.find('a').get('href')
                doiish = "/".join([l for l in link.replace("https://", "").split('/') if re.search(r'\d', l) is not None])
                quote= unidecode(result.find('div', class_='gs_a').get_text())
                authors, journal_year, editor = quote.split(" - ")
                authors = authors.replace("...", ", et. al")
                journal, year = journal_year.split(", ")

                papers.append({
                    "article_id": doiish,
                    "title": title,
                    "link": link,
                    "quote": quote,
                    "authors": authors,
                    "year": year,
                    "journal": journal,
                    "editor": editor
                })


        else:
            print(f'Failed to retrieve search results. Status Code: {response.status_code}')

    except Exception as e:
        print(f'An error occurred: {str(e)}')

    return n, papers


class Command(BaseCommand):

    help = 'Parse Google Scholar citation results page'

    def add_arguments(self, parser):
        parser.add_argument('--start', type=int, help='Start at article number')

    def handle(self, *args, **options):

        n, articles = gs(start=options.get('start', 0))
        for article in articles:
            print(f'Article scraped: {article["article_id"]}')

        # get all db papers
        db = [a.article_id for a in Article.objects.all()]
        articles_to_create = [Article(**a) for a in articles if a["article_id"] not in db]
        r = Article.objects.bulk_create(articles_to_create)
        for paper in r:
            print(f"Adding: {paper}")

        if not options.get('start', 0):
            print(f"Saving {n} citations")
            Citations.objects.create(timestamp=int(time.time()), n=n)
