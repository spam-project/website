# Generated by Django 5.0.1 on 2024-02-26 15:24

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('workshop', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='workshop',
            name='link',
            field=models.CharField(blank=True, max_length=126, null=True),
        ),
    ]
