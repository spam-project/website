from django.db import models
from datetime import date

class Citations(models.Model):
    timestamp = models.IntegerField(default=0)
    n = models.IntegerField(default=0)

    def get_date(self, format="%Y-%m-%d"):
        return date.fromtimestamp(self.timestamp)

class Article(models.Model):
    article_id = models.CharField(max_length=100, primary_key=True)
    title = models.CharField(max_length=256, default="title")
    link = models.CharField(max_length=256, default="link")

    quote = models.CharField(max_length=256, default="quote")
    authors = models.CharField(max_length=256, default="authors")
    year = models.CharField(max_length=256, default="year")
    journal = models.CharField(max_length=256, default="journal")
    editor = models.CharField(max_length=256, default="editor")

    def __str__(self):
        return self.title
