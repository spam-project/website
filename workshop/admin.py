from django.contrib import admin
from .models import Workshop, Developer

def toggle_open_registration(modeladmin, request, queryset):
    for obj in queryset:
        obj.open_registration = not obj.open_registration  # Toggle the boolean field
        obj.save()

toggle_open_registration.short_description = "Open/close registration"

def toggle_display_gallery(modeladmin, request, queryset):
    # reset all display to False
    Workshop.objects.update(display_gallery=False)
    for obj in queryset:
        obj.display_gallery = not obj.display_gallery
        obj.save()

toggle_display_gallery.short_description = "Display/hide gallery"

@admin.register(Workshop)
class WorkshopAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'open_registration', 'display_gallery', 'start', 'end', 'year', 'month', 'country', 'city')
    actions = [toggle_open_registration, toggle_display_gallery]
    
@admin.register(Developer)
class DeveloperAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'name', 'text')

